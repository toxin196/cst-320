﻿using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerController : MonoBehaviour {

    public float speed;
    public AudioSource song1;
    public AudioSource song2;
    public AudioSource num_1_sound;
    public AudioSource num_2_sound;
    public AudioSource num_3_sound;
    public AudioSource num_4_sound;
    public AudioSource num_5_sound;
    public AudioSource num_6_sound;
    public AudioSource num_7_sound;
    public AudioSource num_8_sound;
    public AudioSource num_9_sound;
    public AudioSource num_0_sound;

    public Text problem;
    private int num1;
    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        num1 = 0;
        problem.text = "Waiting for Cube...";

    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {

	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Cube"))
        {

            //song1.mute = !song1.mute;
            //song2.Play();
            num1 = other.gameObject.GetComponent<CubeController>().value;
            if (num1 == 0) { num_0_sound.Play(); } 
            if (num1 == 1) { num_1_sound.Play(); }
            if (num1 == 2) { num_2_sound.Play(); }
            if (num1 == 3) { num_3_sound.Play(); }
            if (num1 == 4) { num_4_sound.Play(); }
            if (num1 == 5) { num_5_sound.Play(); }
            if (num1 == 6) { num_6_sound.Play(); }
            if (num1 == 7) { num_7_sound.Play(); }
            if (num1 == 8) { num_8_sound.Play(); }
            if (num1 == 9) { num_9_sound.Play(); }
            problem.text = "This is Block: " + num1.ToString();
            
        }
        else
        {
            problem.text = "Waiting for Cube...";
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //song1.mute = !song1.mute; 
        problem.text = "Waiting for Cube...";
    }
}
