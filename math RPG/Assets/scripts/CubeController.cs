﻿using System.Collections;
using System.Collections.Generic;
using Leap.Unity.Interaction;
using UnityEngine;
using UnityEngine.Audio;

public class CubeController : MonoBehaviour {
    //Vector3 spawn;
    public UnityEngine.Vector3 cubeCenter;
    public int value;

    // Use this for initialization
    void Start ()
    {
        cubeCenter = transform.localPosition; //gets the starting location
	}

    int getVal()
    {
        return value;
    }

    // Update is called once per frame
    public UnityEngine.Transform cubeTransform;
    public UnityEngine.Rigidbody cubeBody;
    void Update ()
    {
        transform.Rotate(new Vector3(0, 25, 0) * Time.deltaTime);

        Vector3 dir = cubeTransform.localPosition - cubeCenter;
        if (dir.magnitude > 0.05)
        {
            cubeBody.AddForce(dir / dir.magnitude * -98F);
        }

        else
        {
            cubeTransform.localPosition = cubeCenter;
        }
    }

}
